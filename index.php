<!DOCTYPE html>
<html lang="fr-ca">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="La Résidence Albert-le-Grand, voisine du campus de l’Université de Montréal, est destinée aux étudiants de 18-35 ans. La résidence est offerte par les frères Dominicains du couvent Saint-Albert-le-Grand, située au 2715, chemin de la Côte-Sainte-Catherine.">
    <meta name="author" content="Antoine Giraud pour l'Espace Benoit Lacroix">
    <link rel="shortcut icon" href="favicon.ico">
    <title>Résidence Albert-le-Grand</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="js/zoombox/zoombox.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-100720721-1', 'auto');
      ga('send', 'pageview');
    </script>
  </head>
  <body>
    <nav class="navbar" id="navbar">
      <div class="container">
        <a href="#" class="navbar-logo">Résidence Albert-le-Grand</a>
        <div class="navbar-menu">
          <a href="#" class="active"><span class="glyphicon glyphicon-home"></span></a>
          <a href="#qui">Qui ?</a>
          <a href="#infos">Infos</a>
          <a href="#application">Application</a>
          <a href="#quoi">Quoi ?</a>
          <a href="#photos">Photos</a>
        </div>
        <div class="navbar-ico-resp" id="navbar-ico-resp">
        </div>
      </div>
    </nav>
    <section class="content" id="content">
      <section class="section">
        <div class="container">
          <h1 id="about" class="head-title">Résidence Albert-le-Grand</h1>
          <p class="lead">
            Vous êtes étudiant et cherchez un logement ? La Résidence Albert-le-Grand est faite pour vous.<br>
            La Résidence Albert-le-Grand, voisine du campus de l’Université de Montréal, est destinée aux étudiants de 18-35 ans.<br>
            La résidence est offerte par les frères Dominicains du couvent Saint-Albert-le-Grand, située au 2715, chemin de la Côte-Sainte-Catherine.
          </p>
        </div>
      </section>
      <section class="section cover_pic cloitre">
        <div class="container">
          <h2 class="head-title">Couloir du Cloître</h2>
        </div><!-- /.container -->
      </section>
      <section class="section jaune">
        <div class="container">
          <h2 id="qui" class="head-title">Pour qui ?</h2>
          <p class="lead">
            La Résidence est pour les étudiants et chercheurs universitaires<br>
            La Résidence est ouverte à des étudiants de toute tradition culturelle et religieuse.<br>
            Étant située dans une section du couvent des frères Dominicains, la Résidence accueille uniquement des hommes.
          </p>
        </div>
      </section>
      <section class="section cover_pic cafet">
        <div class="container">
          <h2 class="head-title">Le réfectoire</h2>
        </div><!-- /.container -->
      </section>
      <section class="section vert">
        <div class="container">
          <h2 id="quoi" class="head-title">Pour quoi ?</h2>
          <p class="lead">
            La Résidence Albert-le-Grand vous offre la possibilité durant la période de vos études ou de vos recherches de vivre dans un contexte de calme, en voisinage avec la vie communautaire des frères Dominicains.
          </p>
          <p>
            Cette résidence est au coeur de la ville, du campus de l'Université de Montréal, des HEC et de Polytechnique, de l'Hôpital Sainte-Justine, de l'Hôpital général juif, des ligne d'autobus (119, 129, 151 et 161) et du métro (ligne bleue).
          </p>
          <p>
            Le Grand Répertoire du patrimoine bâti de Montréal qualifie le couvent des Dominicains de bâtiment moderne de qualité exceptionnelle. Il est composé de trois volumes principaux disposés en triangle autour d'un cloître. La chapelle et son clocher font face au chemin de la Côte-Sainte-Catherine, la façade borde la rue tandis que l'aile de la Résidence unit, en diagonale, les deux premiers volumes.
          </p>
          <p>
          Vous y trouverez un lieu unique et propice aux études et à la recherche.
          </p>
          <h3>Conditions d’accueil</h3>
          <ul>
              <li>Fournir une preuve d’admission à l’université pour l’année académique concernée.</li>
              <li>Si vous êtes un étudiant international, vous devrez aussi fournir toutes les preuves relatives à votre séjour d’études au Québec : Certificat d’acceptation du Québec (CAQ), permis d’études, visa (si requis par votre pays de résidence).<br>Pour plus d’informations, voir : <a href="http://www.bei.umontreal.ca/bei/form_caq.htm">http://www.bei.umontreal.ca/bei/form_caq.htm</a> et <a href="http://www.cic.gc.ca/francais/information/bureaux/demande-ou.asp">http://www.cic.gc.ca/francais/information/bureaux/demande-ou.asp</a>.</li>
          </ul>
          <p><em>Notez que ces processus sont votre entière responsabilité. La Résidence ne conclura aucune entente sans la présentation des preuves exigées.</em></p>
        </div><!-- /.container -->
      </section>
      <section class="section cover_pic viewCouvent">
        <div class="container">
          <h2 class="head-title">Vue depuis le Mont Royal</h2>
        </div><!-- /.container -->
      </section>
      <section class="section">
        <div class="container">
          <h2 id="infos" class="head-title">Commodités et services</h2>
          <div class="row">
            <div class="col-xs-6">
              <h3>Localisation</h3>
              <dl class="dl-horizontal">
                  <dt>Adresse :</dt>
                  <dd><address>2715, chemin de la Côte-Sainte-Catherine<br>Montréal, Québec, Canada  H3T 1B6</address></dd>
                  <dt>A pied :</dt>
                  <dd>
                    <ul class="list-unstyled">
                        <li>Université de Montréal</li>
                        <li>HEC Montréal</li>
                        <li>École Polytechnique</li>
                    </ul>
                  </dd>
                  <dt>Transports :</dt>
                  <dd>
                    <ul class="list-unstyled">
                        <li>Service d'autobus (ligne no 129) <em>en face de la résidence</em></li>
                        <li>Station de métro Université-de-Montréal (ligne bleue)</li>
                    </ul>
                  </dd>
              </dl>
            </div>
            <div class="col-xs-6">
              <h3>Commodités et services</h3>
              <ul>
                <li>Chambres meublées de 198 pieds carrés (18 m²) ou de 198 pieds carrés (18 m²)</li>
                <li>Électricité et chauffage inclus</li>
                <li>Toilettes et douches en partage sur l’étage</li>
                <li>Salle pour activités communautaires</li>
                <li>Service de cafétéria : trois repas par jour*</li>
                <li>Réfrigérateur et four à micro-ondes dans la cuisinette (possibilité d’avoir un petit réfrigérateur dans la chambre, en sus)</li>
                <li>Internet.</li>
                <li>Service de porterie et casier postal individuel</li>
                <li>Salle de lavage / fer à repasser**</li>
              </ul>
              <p><em>* La Résidence ne peut offrir des régimes alimentaires particuliers.</em></p>
              <p><em>** La literie et les serviettes ne sont pas incluses mais peuvent être louées à 50$ par trimestre.</em></p>
            </div>
          </div>
          <h3>Tarification :</h3>
          <p>Le paiement de votre chambre doit être fait le 1er jour de chaque mois et remis au responsable de la Résidence. Il s'élève à 950 $ CAD par mois, payable en argent comptant ou par chèque émis à l'ordre de <em>Les Dominicains de Saint-Albert</em><br>
            Ce montant inclue tous les repas. Les repas non pris sur place ne peuvent pas être décomptés du forfait global.
          </p>
          <h4>Chambre de 165 pieds carrés (15 m²)</h4>
          <p>925$ par mois</p>
          <h4>Chambre de 198 pieds carrés (18 m²)</h4>
          <p>950$ par mois</p>
          <h3>Information et coordonnées</h3>
          <dl class="dl-horizontal">
            <dt>Courriel :</dt>
            <dd>procure2715@gmail.com</dd>
            <dt>Téléphone :</dt>
            <dd>
              +1-514-731-3603 poste 245 (Carlos Bettancourt) <br>
            </dd>
            <dt>Télécopieur :</dt>
            <dd>+1-514-731-0676</dd>
          </dl>
        </div><!-- /.container -->
      </section>
      <section class="section cover_pic entreeCouventParking">
        <div class="container">
          <h2 class="head-title">Entrée de la résidence</h2>
        </div><!-- /.container -->
      </section>
      <section class="section bleu">
        <div class="container">
          <h1 class="head-title" id="application">Adresse</h1>
          <h3>Pour poser votre candidature :</h3>
          <ol>
            <li>Remplir le <a href="https://docs.google.com/forms/d/e/1FAIpQLSce2lCP47QlUNdJR33qYJ5-ioc2Ych_xvg-xGfmutNaMnX5bw/viewform">formulaire en ligne</a></li>
            <li>Fournir une preuve d’admission à une institution universitaire de Montréal, pour l’année académique concernée</li>
            <li>Prendre connaissance des règlements de la Résidence et signer l’entente avec la Résidence pour la durée du séjour</li>
          </ol>
          <em>La Résidence se réserve le droit de sélectionner les candidats en fonction du nombre de chambres disponibles et des exigences de l’inscription</em>
        </div><!-- /.container -->
      </section>
      <section class="section cover_pic leglise">
        <div class="container">
          <h2 class="head-title">L'Eglise du couvent Saint-Albert-le-Grand</h2>
        </div><!-- /.container -->
      </section>
      <section class="section">
        <div class="container">
          <h1 class="head-title" id="photos">Photos</h1>
          <div class="row">
            <div class="col-xs-6 col-md-2">
              <a href="img/photos_web/Cafétéria_Camille-Lepage.jpg" class="thumbnail zoombox zgallery1" title="La Cafétéria © Camille Lepage">
                <img src="img/photos_web/Cafétéria_Camille-Lepage_210x137.jpg" alt="La Cafétéria © Camille Lepage">
              </a>
            </div>
            <div class="col-xs-6 col-md-2">
              <a href="img/photos_web/Chambre-165-pc.jpg" class="thumbnail zoombox zgallery1" title="Chambre 165 pc">
                <img src="img/photos_web/Chambre-165-pc_210x137.jpg" alt="Chambre 165 pc">
              </a>
            </div>
            <div class="col-xs-6 col-md-2">
              <a href="img/photos_web/Chambre-lavabo.jpg" class="thumbnail zoombox zgallery1" title="Chambre lavabo">
                <img src="img/photos_web/Chambre-lavabo_210x137.jpg" alt="Chambre lavabo">
              </a>
            </div>
            <div class="col-xs-6 col-md-2">
              <a href="img/photos_web/Couloir-cloître_Camille-Lepage.jpg" class="thumbnail zoombox zgallery1" title="Couloir du cloître © Camille Lepage">
                <img src="img/photos_web/Couloir-cloître_Camille-Lepage_210x137.jpg" alt="Couloir du cloître © Camille Lepage">
              </a>
            </div>
            <div class="col-xs-6 col-md-2">
              <a href="img/photos_web/Couvent_arrière_Jessica-Drouin.jpg" class="thumbnail zoombox zgallery1" title="Arrière du couvent © Jessica Drouin">
                <img src="img/photos_web/Couvent_arrière_Jessica-Drouin_210x137.jpg" alt="Arrière du couvent © Jessica Drouin">
              </a>
            </div>
            <div class="col-xs-6 col-md-2">
              <a href="img/photos_web/Église_extérieur_Dragana-Milasin.jpg" class="thumbnail zoombox zgallery1" title="Extérieur de l'église © Dragana Milasin">
                <img src="img/photos_web/Église_extérieur_Dragana-Milasin_210x137.jpg" alt="Extérieur de l'église © Dragana Milasin">
              </a>
            </div>
            <div class="col-xs-6 col-md-2">
              <a href="img/photos_web/eglise_vitraux_Jolyanne-Gagnon.jpg" class="thumbnail zoombox zgallery1" title="Vitraux de l'église © Jolyanne Gagnon">
                <img src="img/photos_web/eglise_vitraux_Jolyanne-Gagnon_210x137.jpg" alt="Vitraux de l'église © Jolyanne Gagnon">
              </a>
            </div>
            <div class="col-xs-6 col-md-2">
              <a href="img/photos_web/Entree-principale_Jolyanne-Gagnon.jpg" class="thumbnail zoombox zgallery1" title="Entrée principale © Jolyanne Gagnon">
                <img src="img/photos_web/Entree-principale_Jolyanne-Gagnon_210x137.jpg" alt="Entrée principale © Jolyanne Gagnon">
              </a>
            </div>
            <div class="col-xs-6 col-md-2">
              <a href="img/photos_web/Vue-du-couven_Audree-Maude-GOSEMICK.jpg" class="thumbnail zoombox zgallery1" title="Vue du couvent © Audree Maude GOSEMICK">
                <img src="img/photos_web/Vue-du-couven_Audree-Maude-GOSEMICK_210x137.jpg" alt="Vue du couvent © Audree Maude GOSEMICK">
              </a>
            </div>
          </div>
        </div><!-- /.container -->
      </section>
      <!-- FOOTER -->
      <footer>
        <!-- <div class="partenaires">
          <div class="container">
            <div class="row">
                <div class="partenaire">
                  <a href="#" title="partenaire" target="_blank">Lien Partenaire</a>
                </div>
            </div>
          </div>
        </div> -->
        <div class="copyright">
          <div class="container">
            <p>© <?= date('Y') ?> - Espace Benoit Lacroix</p>
          </div>
        </div>
      </footer>
    </section>
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/zoombox/zoombox.js"></script>
    <script src="js/app.js"></script>
  </body>
</html>